# NodeFerret Bash Agent

Template files used by NodeFerret to generate install scripts.


## Files

`setup_template.sh` - Fetches 'agent_template.sh' and creates cronjob.

`agent_template.sh` - Is run by cron every minute, posts raw data to NodeFerret.

`autoinstall_template.sh` - Setup script for providers with post-install support.
It automatically creates nodes on nodeferret via API, then runs the appropriate setup_template.sh on the newly created server :sunglasses:  

`removal_template.sh` - Removes agent, crontab and any services from server.

## Setup

#### Requirements
 - systemd OR sysvinit with cron
 - curl

#### Installation

Can be used with the format 

```
curl -sL https://setup_template_path/ | sh
```


This will create a file `nf.sh` at `/opt/nodeferret` and setup a systemd service (or cron if sysvinit) to run every minute


#### Un-Installation

Remove crontab entry

```
crontab -e
```

Delete the agent file in `/usr/local/nodeferret`

```
rm -rf /usr/local/nodeferret
```

## Compatibility

Generally sould work on any Linux machine

Does not (yet) work on FreeBSD or any other BSD based systems

Tested on:

 - CoreOS 1745.6.0, 1800.6.9
 - CentOS 6, 7
 - Debian 8, 9
 - Fedora 27, 28
 - Ubuntu 14.05, 16.04, 18.04
 

Startup Scripts tested on

 - DigitalOcean
 - Vultr
 