#!/bin/sh
#
# NodeFerret Agent Installer (nodeferret.com)

NODE={{ node }}
TOKEN={{ token }}
SERVER={{ server }}
SERVER_PATH=${SERVER}/v1/dl/agent

# Uninstall old versions
printf 'removing old versions\n'
rm -rf /usr/local/nodeferret
rm -rf /opt/nodeferret
rm -rf /etc/systemd/system/nodeferret.*
if [ -x "$(command -v crontab)" ]; then
  crontab -l | grep -v '/nodeferret/nf.sh' | crontab -
fi

# Check if using systemd or init
is_systemd='false'
if ps -p 1 | grep -q 'systemd'; then
  printf 'systemd detected\n'
  is_systemd='true'
fi

# check if cron service is enable
has_cron='false'
if pgrep -x cron > /dev/null || pgrep -x crond > /dev/null; then
  printf 'cron detected\n'
  has_cron='true'
fi

if [ "$is_systemd" = 'false' ] && [ "$has_cron" = 'false' ]; then
  printf 'systemd unavailable. cron unavailiable\n'
  printf 'please install cron and try again\n'
  printf 'Install Failed\n'
  exit 1
fi

printf 'Fetching agent...'
mkdir -p /opt/nodeferret
curl -s ${SERVER_PATH}/${TOKEN}/${NODE}/ --output /opt/nodeferret/nf.sh
chmod u+x /opt/nodeferret/nf.sh
printf 'done\n'

# IF SYSTEMD
if [ "$is_systemd" = 'true' ]; then
  printf 'Using systemd...'

  # Note: Heredoc lines are TAB delimited. Spaces will not work
  cat > /etc/systemd/system/nodeferret.service <<-EOD
	[Unit]
	Description=Runs NodeFerret Agent

	[Service]
	Type=oneshot
	ExecStart=/bin/sh -c '/opt/nodeferret/nf.sh'
EOD

  cat > /etc/systemd/system/nodeferret.timer <<-EOD
	[Unit]
	Description=Run nodeferret.service every minute

	[Timer]
	OnCalendar=*:0/1
	Unit=nodeferret.service

	[Install]
	WantedBy=multi-user.target
EOD

    systemctl daemon-reload
    systemctl start nodeferret.service  # run initial job
    systemctl start nodeferret.timer    # start timer service
    printf 'service created\n'

else

  # NO SYSTEMD, TRY CRON
  if [ "$has_cron" = 'true' ]; then

    printf 'creating crontab entry...'
    (crontab -l 2>/dev/null; printf '*/1 * * * * /bin/sh /opt/nodeferret/nf.sh\n') | crontab -
    printf 'done\n'

    # run initial job
    /bin/sh /opt/nodeferret/nf.sh > /dev/null

  fi  # END has_cron

fi # end is_systemd

printf 'Installation successful\n'
