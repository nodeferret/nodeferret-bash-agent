#!/bin/sh
#
# NodeFerret Agent (nodeferret.com)

# Flags for experimental features
zfsflag='false'
dockerflag='false'
while getopts 'zd' flag; do
  case "${flag}" in
    z) zfsflag='true' ;;
    d) dockerflag='true' ;;
    *) error "Unexpected option ${flag}" ;;
  esac
done

NODE={{ node }}
TOKEN={{ token }}
SERVER={{ server }}
SERVER_PATH=${SERVER}/v1/raw/

# Fetch System info
if [ -f /etc/os-release ]; then
    . /etc/os-release
    operating_system=${NAME}
    version=${VERSION_ID}
elif [ -f /etc/lsb-release ]; then
    . /etc/lsb-release
    operating_system=${DISTRIB_ID}
    version=${DISTRIB_RELEASE}
elif [ -f /etc/system-release ]; then
    operating_system=$(cat /etc/system-release | awk '{print $1}')
    version=$(cat /etc/system-release)
else
    operating_system=$(uname -s)
    version=$(uname -r)
fi

hostname=$( hostname -f )
kernel=$( uname -s )
release=$( uname -r )
architecture=$( uname -m )
ip_list=$( ip -o addr | awk '!/^[0-9]*: ?lo|link\/ether/ {gsub("/", " ") ; print $2, $4}' | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' )
uptime=$( cat /proc/uptime | awk '{print $1}' ) # uptime -s ?

process_count=$( ps aux | wc -l )
file_handles=$( sysctl fs.file-nr | sed 's/\t/  /g' ) # result contains tabs

# Fetch bulky data to process on server
RAW_CPU_CONFIG=$( lscpu | grep -v Flags | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' )
RAW_CPU_DATA=$( cat /proc/stat | grep cpu | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' )
RAW_MEMORY_DATA=$( cat /proc/meminfo | head -n 20 | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' )
RAW_DISK_SIZE_DATA=$( df -Tk | grep -Ee '\S+\s+(ext[234]|vfat|xfs|simfs)' | grep -v -Ee '(^/dev/zd|\S+\s+zfs)' | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' ) # 1k blocks
RAW_DISK_INODES_DATA=$( df -Ti | grep -Ee '\S+\s+(ext[234]|vfat|xfs|simfs)' | grep -v -Ee '(^/dev/zd|\S+\s+zfs)' | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' ) # 1k blocks
RAW_NETWORK_DATA=$( cat /proc/net/dev | grep -v -e 'Inter' -e 'face' | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' ) # Exclude first 2 lines
RAW_PROCESS_CPU_DATA=$( ps aux | grep -v COMMAND | awk '{arr[$11]+=$3} ; END {for (key in arr) print arr[key],key}' | sort -rnk1 | head -n 10 | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' ) # Sort by Cpu Usage
RAW_PROCESS_RAM_DATA=$( ps aux | grep -v COMMAND | awk '{arr[$11]+=$4} ; END {for (key in arr) print arr[key],key}' | sort -rnk1 | head -n 10 | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' ) # Sort by Ram Usage

if [ "${zfsflag}" = 'true' ] ; then
    RAW_ZFS_DATA=$( zpool list | grep -v -Ee '^NAME' | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g'  )
fi

if [ "${dockerflag}" = 'true' ] ; then
    RAW_DOCKER_DATA=$( docker stats --no-stream --no-trunc | sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' )
fi

CONTENT=$(cat << END
{
    "node": ${NODE},

    "hostname": "${hostname}",
    "kernel": "${kernel}",
    "release": "${release}",
    "version": "${version}",
    "architecture": "${architecture}",
    "operating_system": "${operating_system}",

    "uptime": "${uptime}",
    "process_count": "${process_count}",
    "file_handles": "${file_handles}",
    "ip_list": "${ip_list}",

    "cpu_config": "${RAW_CPU_CONFIG}",
    "cpu": "${RAW_CPU_DATA}",
    "ram": "${RAW_MEMORY_DATA}",
    "disk": "${RAW_DISK_SIZE_DATA}",
    "inode": "${RAW_DISK_INODES_DATA}",
    "network": "${RAW_NETWORK_DATA}",
    "process_cpu": "${RAW_PROCESS_CPU_DATA}",
    "process_ram": "${RAW_PROCESS_RAM_DATA}",
    "zfs": "${RAW_ZFS_DATA}",
    "docker": "${RAW_DOCKER_DATA}"
}
END
)

curl -si -H "Content-Type: application/json" -H "Authorization: Token ${TOKEN}" -X POST --data "${CONTENT}" ${SERVER_PATH}

printf '\n'
