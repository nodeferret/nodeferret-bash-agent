#!/bin/sh
#
# NodeFerret Agent Auto-installer (nodeferret.com)

PREFIX={{ prefix }}
TOKEN={{ token }}
SERVER={{ server }}

NODE_PATH=${SERVER}/v1/nodes/
NAME=$(cat /dev/urandom | tr -cd 'a-f0-9' | head -c 6) #RANDOM NAME

CONTENT=$(cat << END
{
    "label": "${PREFIX}-${NAME}",
    "tags": ["autocreated", "$PREFIX"]
}
END
)

NODE=$( curl -s -H "Content-Type: application/json" -H "Authorization: Token ${TOKEN}" -X POST --data "${CONTENT}" ${NODE_PATH} 2>&1 | awk '/"id"/ gsub(":|,", " ") { print $2 }' )
curl -L ${SERVER}/v1/dl/setup/${TOKEN}/$NODE/ | sh
