#!/bin/sh
#
# NodeFerret Agent Uninstaller (nodeferret.com)

if [ -x "$(command -v crontab)" ]; then
  printf 'Removing crontab entry'
  crontab -l | grep -v '/nodeferret/nf.sh' | crontab -
fi

if [ -d /usr/local/nodeferret ]; then
  printf 'Removing agent scripts'
  rm -rf /usr/local/nodeferret
fi

if [ -d /opt/nodeferret ]; then
  printf 'Removing agent scripts'
  rm -rf /opt/nodeferret
fi

if [ -f /etc/systemd/system/nodeferret.service ] || [ -f /etc/systemd/system/nodeferret.timer ]; then
  printf 'Removing systemd service'
  rm -rf /etc/systemd/system/nodeferret.*
fi

if ps -p 1 | grep -q 'systemd'; then
  printf 'Reloading systemd'
  systemctl daemon-reload
fi

printf "Removal completed successfully"